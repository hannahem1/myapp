package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends AppCompatActivity {

    TextView text;
    EditText inputTextOrLoadFile;
    EditText inputSaveFile;
    Context context = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.textView);
        inputTextOrLoadFile = (EditText) findViewById(R.id.textOrLoadFile);
        inputSaveFile = (EditText) findViewById(R.id.saveFile);
        context = MainActivity.this;

        System.out.println("KANSION SIJAINTI1: " + context.getFilesDir());
    }


    public void save(View v){
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput(String.valueOf(inputSaveFile.getText()),Context.MODE_PRIVATE));
            osw.write(String.valueOf(inputTextOrLoadFile.getText()));
            osw.close();
            inputTextOrLoadFile.setText("");
            inputSaveFile.setText("");
        } catch (IOException e){
            Log.e("IOException", "Virhe syötteessä");
        }finally{
            System.out.println("Kirjoitettu tiedostoon");
        }
    }

    public void load(View v) {
        try {
            InputStream ins = context.openFileInput(String.valueOf(inputTextOrLoadFile.getText()));
            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String textLine;
            while((textLine = br.readLine()) != null){
                text.setText(textLine);
            }
            ins.close();
            inputTextOrLoadFile.setText("");
        } catch (IOException e){
            Log.e("IOException", "Virhe syötteessä");
        } finally{
            System.out.println("Luettu");
        }

    }
}